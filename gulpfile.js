var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglifyjs');
var browserify = require('gulp-browserify');
 
gulp.task('build', function() {
  gulp.src([
      './index.js'
    ])
    .pipe(browserify())
    .pipe(concat('realtime.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist'));
});