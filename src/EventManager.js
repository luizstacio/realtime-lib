/**
 *  @module EventManager
 */
var _Util = require('./Util');

function EventManager () {
  var eventBuss = {},
      Util = _Util;

  /**
   *  @method on
   *  @description Add a function in event listener.
   *  @param {String} event - Name of custom event to be listen.
   *  @param {Function} callback - Function for calling in event fired.
   *  @param {Object} callback.data - The data passed for emitter.
   */
  function on (event, fn) {
    if ( !Util.isFunction(fn) ) {
      throw(new Error('On parameteres requires is [event: String, callback: Function] your callback is a ' + Util.toString(event)));
    }
    if ( !Util.isString(event) ) {
      throw(new Error('On parameteres requires is [event: String, callback: Function] your event is a ' + Util.toString(event)));
    }

    if ( !eventBuss[event] ) eventBuss[event] = [];

    eventBuss[event].unshift(fn);
  }

  /**
   *  @method unAll
   *  @description Remove all listeners
   *  @param {String} event - Name of custom event.
   */
  function unAll (event) {
    eventBuss[event] = [];
  }

  /**
   *  @method un
   *  @description Remove a specific listener.
   *  @param {String} event - Name of custom event.
   *  @param {Function} function - Function of listener.
   */
  function un (event, fn) {
    var index;

    if ( !eventBuss[event] ) return;
    
    index = eventBuss[event].indexOf(fn);

    if ( !!~index ) {
      eventBuss[event].splice(index, 1);
    }
  }

  /**
   *  @method emit
   *  @description Emit a specific event
   *  @param {String} event - Event name to be fired.
   *  @param {Object} data - Data to be passed to the fired event.
   */
  function emit (event, object) {
    var eventBussLength, currentEv, i = 0,
        args = Util.toArray(arguments).splice(1);

    try {
      if ( !eventBuss[event] ) return;
      i = eventBussLength = eventBuss[event].length;
      currentEv = eventBuss[event];

      while (i--) {
        if (Util.callback(currentEv[i], args) === false) break;
      }
    } catch (e) { throw new Error(e.message) };
  }

  return {
    on: on,
    un: un,
    unAll: unAll,
    emit: emit
  }
}

module.exports = EventManager;