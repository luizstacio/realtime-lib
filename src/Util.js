/**
 *  @module Util
 */
var Util = {};

/**
 *  @method toString
 *  @description Transform all types in object string
 *  @param {Object} val - Pass the object for transformed.
 *  @return {String} - return string value.
 */
Util.toString = function (val) {
  return Object.prototype.toString.call(val);
}

/**
 *  @method isFunction
 *  @description checks if value passed is a Function.
 *  @param {Object} val
 *  @return {Boolean}
 */
Util.isFunction = function (val) {
  return this.toString(val) === '[object Function]';
}

/**
 *  @method isString
 *  @description checks if value passed is a String.
 *  @param {Object} val
 *  @return {Boolean}
 */
Util.isString = function (val) {
  return this.toString(val) === '[object String]';
}

/**
 *  @method isArray
 *  @description checks if value passed is a Array.
 *  @param {Object} val
 *  @return {Boolean}
 */
Util.isArray = function (val) {
  return this.toString(val) === '[object Array]';
}

/**
 *  @method toArray
 *  @description transforms the value passed for Array.
 *  @param {Object} val
 *  @return {Boolean}
 */
Util.toArray = function (val) {
  return Array.prototype.concat.apply([], val);
}


/**
 *  @method callback
 *  @description verify if fn passed is a function and exec.
 *  @param {Function} fn - function callback.
 *  @param {Object} scope - execution scope.
 */
Util.callback = function (fn, args, scope) {
  return this.isFunction(fn) ? fn.apply(scope || fn, args) : null;
}

/**
 *  @method debounce
 *  @description Create a debounce.
 *  @param {Function} fn - function that needs debounce.
 *  @param {Number} time - time to wait without action.
 *  @return {Function} Executable funcion.
 */
Util.debounce = function (fn, t) {
  var timer;
  
  return function () {
    var args = arguments;

    clearTimeout(timer);
    timer = setTimeout(function () {
      fn.apply(fn, args);
    }, t);
  }
}

/**
 *  @method extend
 *  @description Create a debounce.
 *  @param {Function} fn - function that needs debounce.
 *  @param {Number} time - time to wait without action.
 *  @return {Function} Executable funcion.
 */
Util.extend = function (obj, obj2) {
  if (!obj2) return obj;

  var keys = Object.keys(obj2);

  keys.forEach(function (key) {
    obj[key] = obj2[key];
  });

  return obj;
}

module.exports =  Util;