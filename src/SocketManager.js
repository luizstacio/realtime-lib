/**
 * @module SocketManager
 */
var _Util = require('./Util');
var _EventManager = require('./EventManager');

/**
 *  @method Constructor
 *  @description Create a new WebSocket and mantains the connection.
 *  @param {String} url - url of server socket
 *  @param {Object} [config] - extra configs
 *  @param {Number} config.TIMEOUT_RECONNECT=3000 - time of try reconnect.
 *  @param {Number} config.TIMEOUT_RESTART=250 - time fo using beetwen close old connection and starts other.
 */
function SocketManager (url, config) {
  var _socket,
      EventManager = _EventManager(),
      Util = _Util,
      configs = Util.extend({
        TIMEOUT_RECONNECT: 1500,
        TIMEOUT_RESTART: 250
      }, config);

  function authParams () {
    var params = '?accessKey=' + config.accessKey + '&client=' + config.client;

    if (config.auth) params += '&auth=' + JSON.stringify(config.auth);

    return params;
  }

  /**
   *  @private
   *  @method connect
   *  @description Create connection WebSocket
   */
  function connect () {
    if (config.accessKey == null) throw (new Error('[accessKey] is undefined. For access api get you need one key.'));
    if (config.client == null) throw (new Error('[client] is undefined. This is necessary for indentifing your users.'));

    _socket = new WebSocket(url + authParams());
    addSocketEvents();
    return _socket;
  }

  /**
   *  @private
   *  @method reconnect
   *  @description Recreate connection WebSocket
   */
  var reconnect = Util.debounce(function() {
    setTimeout(connect, configs.TIMEOUT_RECONNECT);
  });

  /**
   *  @private
   *  @method socketOn
   *  @description Add listeners in socket event
   *  @param {String} event - Name of custom event to be listen.
   *  @param {Function} callback - Function for calling in event fired.
   *  @param {Object} callback.data - The data passed for emitter.
   */
  function socketOnAll (callback) {
    EventManager.on('all-messages', callback);
  }

  /**
   *  @private
   *  @method socketOn
   *  @description Add listeners in socket event
   *  @param {String} event - Name of custom event to be listen.
   *  @param {Function} callback - Function for calling in event fired.
   *  @param {Object} callback.data - The data passed for emitter.
   */
  function socketOn (event, callback) {
    EventManager.on(event, callback);
  }

  /**
   *  @private
   *  @method socketUn
   *  @description Remove specific listener in socket event.
   *  @param {String} event - Name of custom event.
   *  @param {Function} function - Function of listener.
   */
  function socketUn (event, callback) {
    EventManager.un(event, callback);
  }
  
  /**
   *  @private
   *  @method socketUnAll
   *  @description Remove specific listener in manager event.
   *  @param {String} event - Name of custom event.
   */
  function socketUnAll (event) {
    EventManager.unAll(event);
  }

  /**
   *  @private
   *  @method socketEmit
   *  @description Fire event in channel on WebSocket.
   *  @param {String} event - Event name to be fired.
   *  @param {Object} data - Data to be passed to the fired event.
   */
  function socketEmit (event, data) {
    if ( !Util.isString(event) ) {
      throw(new Error('[socketEmit(event, data)]: name of event must be String.'));
    }

    if ( !_socket.isOpen ) {
      EventManager.on('waitOpen', socketEmit.bind(this, event, data));
      return;
    }

    _socket.send(JSON.stringify({
      event: event,
      data: data
    }));
  }

  /**
   *  @private
   *  @method onOpenSocket
   *  @description Listen event open of WebSocket
   */
  function onOpenSocket (socket) {
    _socket.isOpen = true;
    _socket.closedByApp = false;

    EventManager.emit('open', socket);
    EventManager.emit('waitOpen');
    EventManager.unAll('waitOpen');
  }

  /**
   *  @private
   *  @method onCloseSocket
   *  @description Listen event close of WebSocket
   */
  function onCloseSocket (socket) {
    _socket.isOpen = false;
    EventManager.emit('close', socket);
    if (!_socket.closedByApp) reconnect();
  }

  /**
   *  @private
   *  @method onErrorSocket
   *  @description Listen event error of WebSocket
   */
  function onErrorSocket (socket) {
    EventManager.emit('error', socket);
    reconnect();
  }

  /**
   *  @private
   *  @method onErrorSocket
   *  @description Listen event message of WebSocket
   */
  function onMessageSocket (socket) {
    var data = JSON.parse(socket.data);
    
    if ( Util.isArray(data) ) {
      data.reverse().forEach(function (d) {
        EventManager.emit(d.event, {
          id: d.id,
          status: d.status,
          data: d.data
        });
        EventManager.emit('all-messages', {
          event: d.event,
          data: d
        });
      });
    } else {
      EventManager.emit(data.event, {
        id: data.id,
        status: data.status,
        data: data.data
      });
      EventManager.emit('all-messages', {
        event: data.event,
        data: data
      });
    }
    
    EventManager.emit('message', socket);
  }


  /**
   *  @method close
   *  @description Close connection.
   */
  function close () {
    _socket.closedByApp = true;
    _socket.close();
  }
  
  /**
   *  @method restart
   *  @description Restart websocket connection.
   */
  function restart (conf) {
    if(_socket.isOpen) close();
    
    Util.extend(config, conf || {});
    
    setTimeout(connect, configs.TIMEOUT_RESTART);
  }

  /**
   *  @private
   *  @method addSocketEvents
   *  @description Delegate the events listeners
   */
  function addSocketEvents () {
    _socket.addEventListener('open', onOpenSocket);
    _socket.addEventListener('close', onCloseSocket);
    _socket.addEventListener('error', onErrorSocket);
    _socket.addEventListener('message', onMessageSocket);
  }

  /* extend Util */
  SocketManager.prototype = Util;
  
  /* Init connection */
  connect();

  return {
    /**
     *  @method unAll
     *  @description unset all listener on event
     *  @param {String} event - Name of custom event to be listen.
     */
    unAll: socketUnAll,
    /**
     *  @method on
     *  @description Listen all events
     *  @param {String} event - Name of custom event to be listen.
     *  @param {Function} callback - Function for calling in event fired.
     *  @param {Object} callback.data - The data passed for emitter.
     */
    onAll: socketOnAll,
    /**
     *  @method on
     *  @description Add listeners in socket event
     *  @param {String} event - Name of custom event to be listen.
     *  @param {Function} callback - Function for calling in event fired.
     *  @param {Object} callback.data - The data passed for emitter.
     */
    on: socketOn,
    /**
     *  @method un
     *  @description Remove specific listener in socket event.
     *  @param {String} event - Name of custom event.
     *  @param {Function} function - Function of listener.
     */
    un: socketUn,
    /**
     *  @method emit
     *  @description Fire event in channel on WebSocket.
     *  @param {String} event - Event name to be fired.
     *  @param {Object} data - Data to be passed to the fired event.
     */
    emit: socketEmit,
    /**
     *  @method close
     *  @description Close connection.
     */
     close: close,
     /**
     *  @method restart
     *  @description Restart websocket connection.
     */
     restart: restart
  };
}

module.exports = SocketManager;